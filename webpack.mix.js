const mix = require('laravel-mix');
require('laravel-mix-eslint');

mix.webpackConfig({
    resolve: {
        extensions: [".js", ".vue"],
        alias: {
            "@": __dirname + "/resources"
        }
    },
    module: {
        rules: [
            {
                test: /\.pug$/,
                oneOf: [
                    {
                        resourceQuery: /^\?vue/,
                        use: ["pug-plain-loader"]
                    },
                    {
                        use: ["raw-loader", "pug-plain-loader"]
                    }
                ]
            }
        ]
    }
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath(path.resolve('./'))
    .js("resources/js/main.js", "static/main.js")
    .sass("resources/scss/styles.scss", "static/css/styles.css")
    .extract([
        "vue",
        "mdbvue"
    ])
    .sourceMaps()
    .version()
