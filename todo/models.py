from django.db import models

class Todo(models.Model):
    done = models.BooleanField(default=False)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    time = models.CharField(max_length=400, default="")

