describe('Integration test for Todo app', () => {
    it('Visits the applications and check if there is data in DB or the proper text appears if not', () => {
        cy.visit('http://localhost:8000')

        cy.get('.card-body').then(($body) => {
            if ($body.text().includes('Add a task with the plus button...')) {
                cy.get('.emptylist')
            } else {
                cy.get('.taskrow').its('length').should('be.gt', 0)
            }
        })
    })

    it('Creates a task than modifies and delete it. Also checking if the proper modal opens', () => {
        // Create a task for testing
        const numOfTasks = Cypress.$('.taskrow').length
        cy.get('.roundbutton').click()
        cy.get('.modal-header').contains('Add New Task')
        cy.get('.form-control').eq(0).type('New task for testing')
        cy.get('.form-control').eq(1).type('Description for testing')
        cy.get('.vdatetime-input').click()
        cy.get('.vdatetime-popup__actions__button--confirm').click().click()
        cy.get('button').contains('Create').click()
        cy.get('.modal').should('not.exist')
        cy.get('.taskrow').its('length').should('eq', numOfTasks + 1)

        // Modify task created for testing
        cy.get('.taskrow').find('.col-8').last().click()
        cy.get('.modal-header').contains('Modify Task')
        cy.get('.form-control').eq(0).clear().type('Modified task')
        cy.get('button').contains('Save').click()
        cy.get('.modal').should('not.exist')
        cy.get('.taskrow').find('.col-8').last().contains('Modified task')

        // Delete task created for testing
        cy.get('.taskrow').find('.col-8').last().click()
        cy.get('button').contains('Delete').click()
        cy.on('window:confirm', () => true)
        cy.get('.modal').should('not.exist')
        cy.get('.card-body').contains('Modified task').should('not.exist')
    })

})