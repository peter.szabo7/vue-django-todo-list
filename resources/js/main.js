import 'babel-polyfill'
import Vue from 'vue'
import Axios from 'axios'
import App from './App.vue'
import { Datetime } from 'vue-datetime'

import '../scss/styles.scss';

import * as mdbvue from 'mdbvue'
for (const component in mdbvue) {
    Vue.component(component, mdbvue[component])
}
Vue.component('datetime', Datetime);

const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
window.axios = Axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['X-CSRFToken'] = csrftoken;

new Vue({
  render: h => h(App)
}).$mount('#app')
