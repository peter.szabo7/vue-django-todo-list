# Daily To-do application with VueJs frontend and Django Backend

## Setting up the project:

1. Clone this repo
2. In project folder run the following commands:
    - npm install
    - npm run dev
    - docker-compose up
3. Open a terminal for the docker container with this command:
    - docker exec -it todo_web bash
4. In the docker terminal use the following command:
    - python manage.py migrate
5. Now the project should be up and running on: localhost:8000
6. Unit & Integration tests can be found:
    - Unit test 
        - location: /todo/tests/tests.py
        - it can be started from the docker terminal with this command: python manage.py test
    - Integration test
        - location: cypress/integration/todoapp_test.js
        - it can be started from the project folder with this command: npx cypress run

